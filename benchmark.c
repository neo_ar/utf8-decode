#if 0
set -euxo pipefail # [^1]
IFS=$'\n\t' # [^1]
tmp=$(mktemp)
function finish { # [^2]
    rm $tmp
}
trap finish EXIT # [^2]
mkdir -p asm
gcc --version > asm/gcc.log
clang --version > asm/clang.log
for i in NAIVE BJOERN TWOTBLS BIGBOI MASKLUT; do
    gcc -g -c -O2 -fno-tree-vectorize -I3rdparty -D$i -o $tmp benchmark.c
    objdump -drwS -Mintel $tmp > asm/gcc_$i
    clang -g -c -O2 -fno-vectorize -fno-slp-vectorize -I3rdparty -D$i -o $tmp benchmark.c
    objdump -drwS -Mintel $tmp > asm/clang_$i
    gcc -O2 -fno-tree-vectorize -I3rdparty -D$i -o gcc_$i benchmark.c
    clang -O2 -fno-vectorize -fno-slp-vectorize -I3rdparty -D$i -o clang_$i benchmark.c
done
exit
# ### Bibliography
# [^1]: Aaron Maxwell, Use Bash Strict Mode (Unless You Love Debugging) http://redsymbol.net/articles/unofficial-bash-strict-mode/
# [^2]: Aaron Maxwell, How "Exit Traps" Can Make Your Bash Scripts Way More Robust And Reliable http://redsymbol.net/articles/bash-exit-traps/
#endif
#define _GNU_SOURCE
#include <sched.h>
#include <sys/stat.h>
#include <x86intrin.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "sugar.h"
static void escape(void* p) // [^1]
{
    asm volatile("" : : "g"(p) : "memory");
}
#if defined (NAIVE)
#include "naiveb.c"
#elif defined (BJOERN)
#include "bjoern.c"
#elif defined (TWOTBLS)
#include "twotbls.c"
#elif defined (BIGBOI)
#include "bigboi.c"
#elif defined (MASKLUT)
#include "masklut.c"
#endif
int main(int argc, char *argv[])
{
    if (argc < 3) exit(EXIT_FAILURE);
    //@NOTE: We pin to first available cpu thread to lessen noise in our profile
    {
        cpu_set_t cpu_set; // [^2, context.cpp#L171]
        if (sched_getaffinity(0, sizeof (cpu_set), &cpu_set))
        {
            perror("failed while getting existing cpu affinity");
            exit(EXIT_FAILURE);
        }
        for (s32 i = 0; i < CPU_SETSIZE; ++i) // [^2, context.cpp#L175]
        {
            if (CPU_ISSET(i, &cpu_set))
            {
                CPU_ZERO(&cpu_set); // [^2, context.cpp#L185]
                CPU_SET(i, &cpu_set);
                if (sched_setaffinity(0, sizeof (cpu_set), &cpu_set))
                {
                    perror("failed to pin");
                    exit(EXIT_FAILURE);
                }
                break;
            }
        }
    }
    u8 * f = argv[1];
    struct stat st;
    stat(f, &st);
    FILE * r = fopen(f, "rb");
    u8 * b = malloc(st.st_size);
    u8 * e = b + st.st_size;
    fread(b, 1, st.st_size, r);
    u32 line_sz = sysconf(_SC_LEVEL1_DCACHE_LINESIZE);
    //@NOTE: --flush is our attempt to get consistent cache behaviour
    //@    : we want to see the impact of initial cache misses
    if (!strcmp(argv[2], "--flush"))
    {
#if defined(NAIVE)
        forTas(u8, utf8_mask_lut, line_sz)
            _mm_clflush(i);
        forTas(u8, utf8_prefix_lut, line_sz)
            _mm_clflush(i);
#else
        forTas(u8, utf8d, line_sz)
            _mm_clflush(i);
#endif
#ifdef TWOTBLS
        forTas(u8, utf8t, line_sz)
            _mm_clflush(i);
#elif defined(MASKLUT) || defined(BIGBOI)
        forTas(u8, mask_lut, line_sz)
            _mm_clflush(i);
#endif
        _mm_mfence();
    }
    sleep(1); //@NOTE: This is to skip capturing the lines above in the profile
              //@  NB: ASSUMING THE CODE ABOVE TAKES LESS THAN 1 SECOND
#ifdef NAIVE
    utf8_to_utf32(b, st.st_size);
#else
    for (; b < e; ++b)
    {
        u32 codepoint = 0;
        for (u32 prev = 0, state = 0; b < e; ++b)
        {
            switch (decode(&state, &codepoint, cast (u32)*b))
            {
                case UTF8_REJECT:
                    codepoint = 0xfffd;
                    if (prev != UTF8_ACCEPT)
                        --b;
                    /* fall through */
                case UTF8_ACCEPT:
                    goto FOUND;
                default:
                    prev = state;
                    break;
            }
        }
        lbl FOUND: escape(&codepoint);
    }
#endif
    return EXIT_SUCCESS;
}
/* ### Bibliography
 * [^1]: Chandler Carruth, Tuning C++: Benchmarks, and CPUs, and Compilers! Oh My! https://www.youtube.com/watch?v=nXaxk27zwlk
 * [^2]: Travis Downs, uarch-bench https://github.com/travisdowns/uarch-bench.git @ ccbebbe */
