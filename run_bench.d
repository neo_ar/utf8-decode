#!/usr/bin/env rdmd
/* @TODO:
 * Use acpi_cpufreq, not intel_pstate
 * `intel_pstate=disable` forces legacy acpi_cpufreq driver (kernel line)
 * `isolcpus=`, `nohz_full=`
 * Disable HT: echo 0 > /sys/devices/system/cpu/cpuN/online
 *      List pairs: /sys/devices/system/cpu/cpuN/topology/thread_siblings_list
 * Boot in recovery mode
 */
void main(string[] args)
{
    enforce(!geteuid(), "This script requires root privs!");
    enum NO_TURBO_FILE = "/sys/devices/system/cpu/intel_pstate/no_turbo"; // [^1, uarch-bench.sh#L11]
    immutable VENDOR_ID = ["lscpu",
              "| grep 'Vendor ID'",
              "| awk '{print $3}'"].esh.strip; // [^1, uarch-bench.sh#L7]
    VENDOR_ID.writeln;
    immutable cores = ["grep processor /proc/cpuinfo",
              "| awk '{print $3}'"].esh
                  .splitLines;
    immutable governors = ["cat",
              "/sys/devices/system/cpu/cpu*/cpufreq/scaling_governor"].esh
                  .splitLines;
    immutable thermald_active = ["systemctl -q is-active thermald"].sh;
    if (thermald_active == 0) ["systemctl stop thermald"].esh;
    scope (exit)
        if (thermald_active == 0) ["systemctl start thermald"].esh;
    ["cpupower -c all frequency-set -g powersave"].esh;
    scope (exit)
        foreach (i, g; governors)
            ["cpupower -c %s".format(i), "frequency-set -g", g].esh;
    string[] original_state;
    auto original_no_turbo = "0"; // [^1, uarch-bench.sh#L34]
    auto write_failed = false;
    if (VENDOR_ID == "GenuineIntel") // [^1, uarch-bench.sh#L117]
    {
        if (NO_TURBO_FILE.exists) // [^1, uarch-bench.sh#L67]
        {
            original_no_turbo = NO_TURBO_FILE.readText;
            if (original_no_turbo != "1")
            {
                try
                {
                    scope (failure) write_failed = true;
                    NO_TURBO_FILE.write("1");
                }
                catch (FileException e)
                    goto msr_disable;
                stderr.writeln("WARN: Use acpi_cpufreq instead of intel_pstate for stable core freq!");
            }
        }
        else
        {
msr_disable:
            if ([`lsmod | grep -q "^msr "`].sh)
                ["modprobe msr"].esh; // [^1, uarch-bench.sh#L18]
            original_state = cores // [^1, uarch-bench.sh#L33]
                .map!(core => ["rdmsr -p", core, "0x1a0 -f 38:38"].esh)
                .array; // [^1, uarch-bench.sh#L22]
            foreach (core; cores) // [^1, uarch-bench.sh#L91]
                ["wrmsr -p", core, "0x1a0 0x4000850089"].esh;
        }
    }
    scope (exit)
    {
        if (VENDOR_ID == "GenuineIntel") // [^1, uarch-bench.sh#L117]
        {
            if (NO_TURBO_FILE.exists // [^1, uarch-bench.sh#L37]
                    && original_no_turbo != "1"
                    && !write_failed)
                NO_TURBO_FILE.write(original_no_turbo);
            else
                cores.each!((i, core) { if (original_state[i] == "0")
                        ["wrmsr -p", core, "0x1a0 0x850089"].esh; });
        }
    }
    "log".mkdirRecurse;
    "log/system.log"
        .write(["inxi -xxx -a -C -f -M -S -z"].esh);
    foreach (i; ["NAIVE", "BJOERN", "TWOTBLS", "BIGBOI", "MASKLUT"])
        foreach (j; ["gcc", "clang"])
            foreach (k; ["small", "medium", "large"])
                ["perf stat -d -d -d -r 100 -D 1000 -o log/%s_%s.%s.log"
                    .format(j, i, k),
                    "-- ./%s_%s %s.txt --flush"
                        .format(j, i, k)].esh;
}
int sh(string[] cmd)
{
    auto c = cmd.join(" ");
    c.writeln;
    auto o = c.executeShell;
    return o.status;
}
string esh(string[] cmd)
{
    auto c = cmd.join(" ");
    c.writeln;
    auto o = c.executeShell;
    enforce(!o.status, o.output);
    return o.output;
}
import core.sys.posix.unistd : geteuid;
import std.algorithm;
import std.array;
import std.exception;
import std.file, std.file : write;
import std.format;
import std.process;
import std.stdio;
import std.string;
/* ### Bibliography */
/* [^1]: Travis Downs, uarch-bench https://github.com/travisdowns/uarch-bench.git @ ccbebbe */
