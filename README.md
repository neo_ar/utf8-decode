# Notes on Deriving a Table-Driven DFA

These notes are my attempt at making sense of a general plan of attack to create
an elegant table driven solution to any problem that can be treated as a reasonably
small DFA. After being blown away seeing Bjoern Hoehrmann's UTF-8 decoder, I knew
I had to study it to glean wisdom of how one could ever come up with such an
elegant solution.

NB: I am not an expert when it comes to reasoning about performance on a
microarchitectural level, so take all of my perf reasoning with a grain of salt.

## Trace out every execution path (you really want to graph it)

UTF-8 Example

```mermaid
graph LR
    a[Check a byte] -. "{C0, C1, F5-FF}" .-> e0
    a -. "{80-BF}" .-> e1
    d0 -. no .-> e2
    d1 -. no .-> e3
    d2 -. no .-> e4
    d3 -. no .-> e2
    d4 -. no .-> e3
    d5 -. no .-> e4
    d6 -. no .-> e2
    subgraph Valid
        a -- "{00-7F}" --> a
        a -- "{C2-DF}" --> d0{"80-BF?"}
        a -- "{E0}" --> d1{"A0-BF?"}
        a -- "{ED}" --> d2{"80-9F?"}
        a -- "{E1-EC, EE, EF}" --> d3{"80-BF?"}
        a -- "{F0}" --> d4{"90-BF?"}
        a -- "{F4}" --> d5{"80-8F?"}
        a -- "{F1-F3}" --> d6{"80-BF?"}
        d0 -- yes --> a
        d1 -- yes --> d0
        d2 -- yes --> d0
        d3 -- yes --> d0
        d4 -- yes --> d3
        d5 -- yes --> d3
        d6 -- yes --> d3
    end
    subgraph Invalid
        e0[Invalid leader]
        e1[Unexpected continuation]
        e2[Invalid continuation]
        e3[Overlong/invalid]
        e4[Out of range]
    end
```

## Map classes, states & non-overlapping transitions to arbitrary numbers

The naive approach would be to go straight to making a transition table, but 
that would require 256 entries for each state in our case. By first mapping bytes
to classes, we reduce our transition table to a mere 96 bytes. Two tables 
totalling 352 bytes vs one table totalling 2048 bytes - both options easily fit
within modern L1 D cache. The large table would take longer to get fully mapped 
into the cache whereas two tables will require 2 loads that can't be issued in 
the same cycle each iteration. L1 hits are cheap while main memory is not, so
it makes sense to prefer the two table solution. Not only that, but as we will 
see the byte to class table will aid in computing a mask necessary for utf8 
decoding, whereas the single table would not.

For now the mapping does not matter. However, it *is* important to find a good
mapping later! It is helpful to create a table like this *listing the ranges* of
each class as it will help a lot later finding a good mapping.

|     Class     | # |  Range           |
|---------------|---|------------------|
|   One byte    | 0 | {00-7F}          |
|   Error       | 1 | {C0,C1,F5-FF}    |
|   Two bytes   | 2 | {C2-DF}          |
|   Three bytes | 3 | {E1-EC,EE,EF}    |
|   Four bytes  | 4 | {F1-F3}          |
|   Low cont.   | 5 | {80-8F}          |
|   Mid cont.   | 6 | {90-9F}          |
|   High cont.  | 7 | {A0-BF}          |
|   E0 byte     | 8 | {E0}             |
|   ED byte     | 9 | {ED}             |
|   F0 byte     | a | {F0}             |
|   F4 byte     | b | {F4}             |

Note that we broke up the continuation range into non-overlapping sections so
that we can distinguish the different ranges that the special case states need.

|     State    | # |             Notes                |
|--------------|---|----------------------------------|
|   Valid      | 0 | successfully found a codepoint   |
|   Invalid    | 1 | did not find a valid codepoint   |
|   One more   | 2 |                                  |
|   Two more   | 3 |                                  |
|   E0 case    | 4 | special case                     |
|   ED case    | 5 | special case                     |
|   F0 case    | 6 | special case                     |
|   Three more | 7 |                                  |
|   F4 case    | 8 | special case                     |

In general it should be fine for the mapping of states to be arbitrary as taking
a transition should be the last thing you do in an iteration of the DFA. The
mapping shown here is what Bjoern chose for his decoder.

## Draw out your byte-to-class table

                  LOW NIBBLE
        0 1 2 3 4 5 6 7 8 9 A B C D E F
      0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
      1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    H 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    I 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    G 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    H 5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
      6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    N 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    I 8 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5 5
    B 9 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6 6
    B A 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7
    L B 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7
    E C 1 1 2 2 2 2 2 2 2 2 2 2 2 2 2 2
      D 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
      E 8 3 3 3 3 3 3 3 3 3 3 3 3 9 3 3
      F a 4 4 4 b 1 1 1 1 1 1 1 1 1 1 1

## Given a class and a previous state, we can draw a transition table

                 CLASS
        0 1 2 3 4 5 6 7 8 9 A B
      0 0 1 2 3 7 1 1 1 4 5 6 8
      1 1 1 1 1 1 1 1 1 1 1 1 1
    S 2 1 1 1 1 1 0 0 0 1 1 1 1
    T 3 1 1 1 1 1 2 2 2 1 1 1 1
    A 4 1 1 1 1 1 1 1 2 1 1 1 1
    T 5 1 1 1 1 1 2 2 1 1 1 1 1
    E 6 1 1 1 1 1 1 3 3 1 1 1 1
      7 1 1 1 1 1 3 3 3 1 1 1 1
      8 1 1 1 1 1 3 1 1 1 1 1 1

state = transition[state*12 + class]

Note: if we pad our table to a power of 2 we can turn the multiply into a shift,
but it is even better to pre-bake the multiplication into the table itself

## Padded table (not as good as Pre-multiplied table)

                 CLASS
        0 1 2 3 4 5 6 7 8 9 A B C D E F
      0 0 1 2 3 7 1 1 1 4 5 6 8 1 1 1 1
      1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    S 2 1 1 1 1 1 0 0 0 1 1 1 1 1 1 1 1
    T 3 1 1 1 1 1 2 2 2 1 1 1 1 1 1 1 1
    A 4 1 1 1 1 1 1 1 2 1 1 1 1 1 1 1 1
    T 5 1 1 1 1 1 2 2 1 1 1 1 1 1 1 1 1
    E 6 1 1 1 1 1 1 3 3 1 1 1 1 1 1 1 1
      7 1 1 1 1 1 3 3 3 1 1 1 1 1 1 1 1
      8 1 1 1 1 1 3 1 1 1 1 1 1 1 1 1 1

state = transition[(state<<4) + class]

## Pre-multiplied table

        0  1  2  3  4  5  6  7  8  9  A  B
    00 00 0C 18 24 54 0C 0C 0C 30 3C 48 60
    0C 0C 0C 0C 0C 0C 0C 0C 0C 0C 0C 0C 0C
    18 0C 0C 0C 0C 0C 00 00 00 0C 0C 0C 0C
    24 0C 0C 0C 0C 0C 18 18 18 0C 0C 0C 0C
    30 0C 0C 0C 0C 0C 0C 0C 18 0C 0C 0C 0C
    3C 0C 0C 0C 0C 0C 18 18 0C 0C 0C 0C 0C
    48 0C 0C 0C 0C 0C 0C 24 24 0C 0C 0C 0C
    54 0C 0C 0C 0C 0C 24 24 24 0C 0C 0C 0C
    60 0C 0C 0C 0C 0C 24 0C 0C 0C 0C 0C 0C

state = transition[state + class]

## Finding a good mapping

What mapping you want to use is entirely dependent on the application in question.
As for UTF-8, our goal is to extract bits from our codeunits to reconstruct the
codepoint.

In pseudo-code:

```c
    class = codeunit_to_class[codeunit];
    if (state == START)
        codepoint = codeunit & mask;
    else
        codepoint = codepoint<<6 | codeunit&0x3F;
    state = transition[state + class];
    if (state == ERROR)
        codepoint = U+FFFD;
```

Mask is {0x7F, 0x1F, 0xF, 0x7} indexed by expected number of codeunits.
We can determine expected number of codeunits from class.

Thus, we want 1-byte encodings to map to 0x7F, 2-byte encodings to map to 0x1F,
3-byte encodings to map to 0xF, and 4-byte encodings to map to 0x7. If that was
the literal mapping, we would have `codepoint = codeunit & class`.
Because class acts as an index into our transition table, we would prefer
to keep the range much smaller than that, and there are more classes leading to
multi-byte encodings than there are masks, so we won't be able to keep it
*that* clean.

We could simply create another lookup table that maps class to mask. Our first
access will likely miss the cache taking on the order of 300 cycles to complete.
On subsequent iterations, we are bound by the load throughput. Assuming L1 cache
hits, we have 2 loads per cycle to work with. Our code is currently doing 2 loads
in a carried dependency chain (transition lookup depends on class lookup) so we
can't expect them to run in the same cycle. By that logic, I would expect class
to mask lookup to be paired with transition lookup. If we use a class to mask
lookup table, we don't need to worry about our mapping and can stop here (seems
worthwhile?) Otherwise, we might want to determine an efficient mapping to make
computing the mask cheap & fast.

TODO: benchmark a class to mask LUT vs good mapping optimisation detailed below

Let's recall our table from earlier:

|     Class     | # |  Range          |
|---------------|---|-----------------|
|   One byte    | 0 | {00-7F}         |
|   Error       | 1 | {C0,C1,F5-FF}   |
|   Two bytes   | 2 | {C2-DF}         |
|   Three bytes | 3 | {E1-EC,EE,EF}   |
|   Four bytes  | 4 | {F1-F3}         |
|   Low cont.   | 5 | {80-8F}         |
|   Mid cont.   | 6 | {90-9F}         |
|   High cont.  | 7 | {A0-BF}         |
|   E0 byte     | 8 | {E0}            |
|   ED byte     | 9 | {ED}            |
|   F0 byte     | a | {F0}            |
|   F4 byte     | b | {F4}            |

This is the part where you need to get clever. Bjoern's cleverness was as follows:

For the purposes of masking, if you pay close attention to the ranges there are
fungible bits to work with:

|             |    mask     |    value    |  fungible   |     notes            |
|-------------|-------------|-------------|-------------|----------------------|
| One byte    |`0b0111_1111`|`0b0xxx_xxxx`|`0b*111_1111`|                      |
| Error       |`0b0000_0000`|`0b1100_000x`|`0b****_****`| mask doesn't matter* |
|             |             |`0b1111_xxxx`|             | 0xF5-0xFF            |
| Two bytes   |`0b0001_1111`|`0b110x_xxxx`|`0b00*1_1111`|                      |
| Three bytes |`0b0000_1111`|`0b1110_xxxx`|`0b000*_1111`|                      |
| Four bytes  |`0b0000_0111`|`0b1111_00xx`|`0b0000_**11`| only 0xF1-0xF3 !!!   |
| Low cont.   |`0b0000_0000`|`0b1000_xxxx`|`0b****_****`| mask doesn't matter* |
| Mid cont.   |`0b0000_0000`|`0b1001_xxxx`|`0b****_****`| mask doesn't matter* |
| High cont.  |`0b0000_0000`|`0b101x_xxxx`|`0b****_****`| mask doesn't matter* |
| E0 byte     |`0b0000_1111`|`0b1110_0000`|`0b000*_****`| no value bits        |
| ED byte     |`0b0000_1111`|`0b1110_1101`|`0b000*_11*1`|                      |
| F0 byte     |`0b0000_0111`|`0b1111_0000`|`0b0000_****`| no value bits, 0xF0  |
| F4 byte     |`0b0000_0111`|`0b1111_0100`|`0b0000_*1**`| 0xF4                 |

    * mask doesn't matter because we signal these via transitioning to Error

This gives us extra combinations to work with.

Of all possible combinations, we must pick a valid set of masks.
Furthermore, we must pick a mapping of our classes to the 0-b range.
Our goal is to pick a set of masks and an ordering of our classes, such that we
can compute the mask from the class with a short, efficient instruction sequence.

Bjoern found the following:

    mask = 0xff >> class;

|             | # |    mask     |  fungible   |
|-------------|---|-------------|-------------|
| One byte    | 0 |`0b1111_1111`|`0b*111_1111`|
| Low cont.   | 1 |`0b0111_1111`|`0b****_****`|
| Two bytes   | 2 |`0b0011_1111`|`0b00*1_1111`|
| Three bytes | 3 |`0b0001_1111`|`0b000*_1111`|
| ED byte     | 4 |`0b0000_1111`|`0b000*_11*1`|
| F4 byte     | 5 |`0b0000_0111`|`0b0000_*1**`|
| Four bytes  | 6 |`0b0000_0011`|`0b0000_**11`|
| High cont.  | 7 |`0b0000_0001`|`0b****_****`|
| Error       | 8 |`0b0000_0000`|`0b****_****`|
| Mid cont.   | 9 |`0b0000_0000`|`0b****_****`|
| E0 byte     | a |`0b0000_0000`|`0b000*_****`|
| F0 byte     | b |`0b0000_0000`|`0b0000_****`|

On x86_64 this will do `sar eax, cl` which uses 3 uops on port 6 with a latency
of 1 or 2 cycles (on Skylake) according to uops.info. Given pipelining I would 
expect this to perform roughly the same as a LUT in L1 cache but the LUT has the
initial overhead of getting the table into cache so I would expect the shift to 
crush the table in performance for small strings, although this overhead may be 
masked if the processor is able to speculatively load the table early enough. 
Additionally, the shift is better in terms of memory usage (naturally). The 
hidden cost here is development time - a LUT is brain-dead simple whereas finding
a set of masks that leads to the shift is not, it requires careful analysis and 
clever thinking. Does that optimisation pay off? Only if utf-8 decoding is a 
bottleneck, in which case you could instead focus that effort on something like 
a SIMD approach, yielding bigger wins. What Bjoern found is elegant and 
instructive but arguably a case of premature optimisation. We will benchmark this
against a LUT to see how they compare in practice.

## Redo byte to class and transition tables to use the good mapping

|     Class     | # |  Range          |
|---------------|---|-----------------|
|   One byte    | 0 | {00-7F}         |
|   Low cont.   | 1 | {80-8F}         |
|   Two bytes   | 2 | {C2-DF}         |
|   Three bytes | 3 | {E1-EC,EE,EF}   |
|   ED byte     | 4 | {ED}            |
|   F4 byte     | 5 | {F4}            |
|   Four bytes  | 6 | {F1-F3}         |
|   High cont.  | 7 | {A0-BF}         |
|   Error       | 8 | {C0,C1,F5-FF}   |
|   Mid cont.   | 9 | {90-9F}         |
|   E0 byte     | a | {E0}            |
|   F0 byte     | b | {F0}            |

                  LOW NIBBLE
        0 1 2 3 4 5 6 7 8 9 A B C D E F
      0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
      1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    H 2 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    I 3 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    G 4 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    H 5 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
      6 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    N 7 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
    I 8 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
    B 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9 9
    B A 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7
    L B 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7 7
    E C 8 8 2 2 2 2 2 2 2 2 2 2 2 2 2 2
      D 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
      E a 3 3 3 3 3 3 3 3 3 3 3 3 4 3 3
      F b 6 6 6 5 8 8 8 8 8 8 8 8 8 8 8

                 CLASS
        0 1 2 3 4 5 6 7 8 9 A B
      0 0 1 2 3 5 8 7 1 1 1 4 6
      1 1 1 1 1 1 1 1 1 1 1 1 1
    S 2 1 0 1 1 1 1 1 0 1 0 1 1
    T 3 1 2 1 1 1 1 1 2 1 2 1 1
    A 4 1 1 1 1 1 1 1 2 1 1 1 1
    T 5 1 2 1 1 1 1 1 1 1 2 1 1
    E 6 1 1 1 1 1 1 1 3 1 3 1 1
      7 1 3 1 1 1 1 1 3 1 3 1 1
      8 1 3 1 1 1 1 1 1 1 1 1 1

        0  1  2  3  4  5  6  7  8  9  A  B
    00 00 0C 18 24 3C 60 54 0C 0C 0C 30 48
    0C 0C 0C 0C 0C 0C 0C 0C 0C 0C 0C 0C 0C
    18 0C 00 0C 0C 0C 0C 0C 00 0C 00 0C 0C
    24 0C 18 0C 0C 0C 0C 0C 18 0C 18 0C 0C
    30 0C 0C 0C 0C 0C 0C 0C 18 0C 0C 0C 0C
    3C 0C 18 0C 0C 0C 0C 0C 0C 0C 18 0C 0C
    48 0C 0C 0C 0C 0C 0C 0C 24 0C 24 0C 0C
    54 0C 24 0C 0C 0C 0C 0C 24 0C 24 0C 0C
    60 0C 24 0C 0C 0C 0C 0C 0C 0C 0C 0C 0C

## Final implementation

```c
    #define UTF8_ACCEPT 0
    #define UTF8_REJECT 12
    inline
    proc u32 utf8_decode(u32 * state, u32 * codepoint, u32 codeunit)
    {
        persistent u8 codeunit_to_class [] = {
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
             1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
             9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,
             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
             7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,
             8,8,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
             2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
            10,3,3,3,3,3,3,3,3,3,3,3,3,4,3,3,
            11,6,6,6,5,8,8,8,8,8,8,8,8,8,8,8
        };
        persistent u8 transition [] = {
             0,12,24,36,60,96,84,12,12,12,48,72,
            12,12,12,12,12,12,12,12,12,12,12,12,
            12, 0,12,12,12,12,12, 0,12, 0,12,12,
            12,24,12,12,12,12,12,24,12,24,12,12,
            12,12,12,12,12,12,12,24,12,12,12,12,
            12,24,12,12,12,12,12,12,12,24,12,12,
            12,12,12,12,12,12,12,36,12,36,12,12,
            12,36,12,12,12,12,12,36,12,36,12,12,
            12,36,12,12,12,12,12,12,12,12,12,12
        };
        assert(state);
        assert(codepoint);
        u32 class = codeunit_to_class[codeunit];
        *codepoint = *state
            ? *codepoint<<6 | codeunit&0x3F;
            : codeunit & (0xff>>class);
        *state = transition[*state + class];
        //@NOTE: Bjoern leaves error handling to the user in his implementation
        return *state;
    }
    /* @EXAMPLE: popping a codepoint off a utf-8 str w/ error handling
        u32 codepoint = 0;
        for (u32 prev = 0, state = 0; *str; ++str)
        {
            switch (utf8_decode(&state, &codepoint, cast (u32)*str))
            {
                case UTF8_REJECT:
                    codepoint = 0xfffd;
                    if (prev != UTF8_ACCEPT)
                        --str;
                    // fall through
                case UTF8_ACCEPT:
                    goto FOUND;
                default:
                    prev = state;
                    break;
            }
        }
        lbl FOUND: ;
    */
```

## Unexplained features of Bjoern's UTF-8 decoder

Bjoern uses a single `utf8d` table instead of two separate tables. I don't know
whether this has any performance implications (alignment for example). I presume
it was merely a stylistic choice. TODO: check in godbolt to make sure this gets optimised to the same thing

The `utf8d` table is declared as a global at top-level rather than within the
decoder procedure with static storage duration. Afaik this is only a question of
scoping. Having it global makes sense if you can think of a reason to use the 
table(s) outside of the decoder itself - I could see the byte to class table 
being useful in other utf8 related procedures for example.

Instead of writing

```c
    *codepoint = *state
        ? *codepoint<<6 | codeunit&0x3F;
        : codeunit & (0xff>>class);
```

Bjoern instead writes

```c
    *codep = (*state != UTF8_ACCEPT) ?
        (byte & 0x3fu) | (*codep << 6) :
        (0xff >> type) & (byte);
```

which appears to be a stylistic choice as `|` and `&` are commutative.
