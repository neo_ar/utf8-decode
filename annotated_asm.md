# Appendix A
*Hand Annotated ASM Listings*

Full objdumps can be found in [asm/](/asm).

## Index
1. [Naive](#naive)
    1. [Clang](#clang-naive)
    2. [GCC](#gcc-naive)
2. [Bjoern](#bjoern)
    1. [Clang](#clang-bjoern)
    2. [GCC](#gcc-bjoern)
3. [Two Tables](#two-tables)
    1. [Clang](#clang-two-tables)
    2. [GCC](#gcc-two-tables)
4. [Mask LUT](#mask-lut)
    1. [Clang](#clang-mask-lut)
    2. [GCC](#gcc-mask-lut)
5. [BIGBOI](#bigboi)
    1. [Clang](#clang-bigboi)
    2. [GCC](#gcc-bigboi)

---

## Naive
### Clang Naive
```nasm
_1f0:	jmp    _211
_200:	mov    QWORD PTR [rsp],r14          ; escape(&codeunit)
_204:	add    rbx,0x1                      ; ++i
_208:	cmp    rbx,rax
_20b:	jae    _356                         ; while (i < e)
_211:	movzx  ecx,BYTE PTR [rbx]
_214:	mov    BYTE PTR [rsp+0x1f],cl
_218:	test   cl,cl
_21a:	jns    _200                         ; if (codeunit <= 0x7F)
_21c:	movzx  esi,BYTE PTR [rcx+r10*1]     ; continuation_length = utf8_prefix_lut[codeunit]
_221:	movzx  edx,BYTE PTR [rsi+r11*1]     ; utf8_mask_lut[continuation_length]
_226:	and    dl,cl                        ; codepoint = codeunit & ...
_228:	movzx  edi,dl
_22b:	mov    DWORD PTR [rsp+0x18],edi
_22f:	lea    edx,[rcx+0x20]               ; edx = codeunit + 0x20
_232:	cmp    dl,0x14
_235:	ja     _2ab                         ; edx > 0x14
; Clang performed a range analysis to skip the switch if codeunit is not within E0-F4
_237:	mov    ecx,0xa0                     ; case 0xE0: maybe_invalid_min = 0xa0
; Clang pulled the first case out of the switch except for the goto target
; (which was placed in the jump table) to save an indirection.
_23c:	mov    ebp,0xbf                     ; maybe_invalid_max = 0xbf
_241:	movzx  edx,dl
_244:	movsxd rdx,DWORD PTR [r15+rdx*4]
_248:	add    rdx,r15
_24b:	jmp    rdx                          ; switch (codeunit)
_24d:	mov    ecx,0x80                     ; case 0xED: maybe_invalid_min = 0x80
_252:	mov    ebp,0x9f                     ; maybe_invalid_max = 0x9f
_257:	jmp    _26a                         ; goto MAYBE_INVALID
_259:	mov    ecx,0x90                     ; case 0xF0: maybe_invalid_min = 0x90
_25e:	jmp    _26a                         ; goto MAYBE_INVALID
_260:	mov    ecx,0x80                     ; case 0xF4: maybe_invalid_min = 0x80
_265:	mov    ebp,0x8f                     ; maybe_invalid_max = 0x8f
_26a:	movzx  edx,BYTE PTR [rbx+0x1]       ; MAYBE_INVALID: continuation = i[1]
_26e:	add    rbx,0x1                      ; ++i
_272:	test   edx,edx
_274:	je     _340                         ; !continuation
_27a:	cmp    ecx,edx
_27c:	ja     _340                         ; maybe_invalid_min > continuation
_282:	cmp    ebp,edx
_284:	jb     _340                         ; maybe_invalid_max < continuation
_28a:	mov    ecx,edi
_28c:	shl    ecx,0x6                      ; codepoint << 6
_28f:	and    edx,0x3f                     ; continuation & 0x3f
_292:	mov    edi,edx
_294:	or     edi,ecx                      ; |
_296:	mov    DWORD PTR [rsp+0x18],edi     ; codepoint =
_29a:	cmp    sil,0x2                      ; switch (continuation_length)
_29e:	je     _304                         ; case 2: goto CASE1
_2a0:	cmp    sil,0x3
_2a4:	je     _2e4                         ; case 3: goto CASE2
_2a6:	jmp    _208
_2ab:	cmp    sil,0x3
_2af:	ja     _324                         ; continuation_length > 3
_2b1:	movzx  ecx,sil
_2b5:	movsxd rcx,DWORD PTR [r8+rcx*4]
_2b9:	add    rcx,r8
_2bc:	jmp    rcx                          ; switch (continuation_length)
_2be:	add    rbx,0x1                      ; case 0: ++i
_2c2:	jmp    _340                         ; goto INVALID_CODEPOINT
_2c4:	movzx  ecx,BYTE PTR [rbx+0x1]       ; case 3: continuation = i[1]
_2c8:	add    rbx,0x1                      ; ++i
_2cc:	test   cl,cl
_2ce:	jns    _340                         ; continuation < UTF8_CONTINUATION_MIN
_2d0:	cmp    cl,0xbf
_2d3:	ja     _340                         ; continuation > UTF8_CONTINUATION_MAX
_2d5:	movzx  ecx,cl
_2d8:	shl    edi,0x6                      ; codepoint << 6
_2db:	and    ecx,0x3f                     ; continuation & 0x3f
_2de:	or     edi,ecx                      ; |
_2e0:	mov    DWORD PTR [rsp+0x18],edi     ; codepoint =
_2e4:	movzx  ecx,BYTE PTR [rbx+0x1]       ; case 2: CASE2: continuation = i[1]
_2e8:	add    rbx,0x1                      ; ++i
_2ec:	test   cl,cl
_2ee:	jns    _340                         ; continuation < UTF8_CONTINUATION_MIN
_2f0:	cmp    cl,0xbf
_2f3:	ja     _340                         ; continuation > UTF8_CONTINUATION_MAX
_2f5:	movzx  ecx,cl
_2f8:	shl    edi,0x6                      ; codepoint << 6
_2fb:	and    ecx,0x3f                     ; continuation & 0x3f
_2fe:	or     edi,ecx                      ; |
_300:	mov    DWORD PTR [rsp+0x18],edi     ; codepoint =
_304:	movzx  ecx,BYTE PTR [rbx+0x1]       ; case 1: CASE1: continuation = i[1]
_308:	add    rbx,0x1                      ; ++i
_30c:	test   cl,cl
_30e:	jns    _340                         ; continuation < UTF8_CONTINUATION_MIN
_310:	cmp    cl,0xbf
_313:	ja     _340                         ; continuation > UTF8_CONTINUATION_MAX
_315:	movzx  ecx,cl
_318:	shl    edi,0x6                      ; codepoint << 6
_31b:	and    ecx,0x3f                     ; continuation & 0x3f
_31e:	or     ecx,edi                      ; |
_320:	mov    DWORD PTR [rsp+0x18],ecx     ; codepoint =
_324:	mov    QWORD PTR [rsp+0x8],r9       ; escape(&codepoint)
_329:	add    rbx,0x1                      ; ++i
_32d:	cmp    rbx,rax
_330:	jb     _211                         ; continue
_336:	jmp    _356
_340:	mov    DWORD PTR [rsp+0x18],0xfffd  ; INVALID_CODEPOINT: codepoint = 0xfffd
_348:	mov    QWORD PTR [rsp+0x10],r9      ; escape(&codepoint)
_34d:	cmp    rbx,rax
_350:	jb     _211                         ; continue
```

### GCC Naive
```nasm
```

## Bjoern
### Clang Bjoern
```nasm
_1a0:	jmp    _1cd
_1a2:	lea    rcx,[rbx-0x1]                ; case UTF8_REJECT: rcx = b - 1
_1a6:	test   edi,edi
_1a8:	mov    DWORD PTR [rsp+0xc],0xfffd   ; codepoint = 0xfffd
_1b0:	cmovne rbx,rcx                      ; if (prev != UTF8_ACCEPT) b = rcx
_1c0:	mov    QWORD PTR [rsp],r8           ; FOUND:
_1c4:	add    rbx,0x1                      ; ++b
_1c8:	cmp    rbx,r14
_1cb:	jae    _23a                         ; b >= e
_1cd:	mov    DWORD PTR [rsp+0xc],0x0      ; codepoint = 0
_1d5:	cmp    rbx,r14
_1d8:	jae    _1c0                         ; b >= e
_1da:	xor    esi,esi                      ; prev = 0
_1dc:	xor    edi,edi                      ; state = 0
_1e0:	movzx  edx,BYTE PTR [rbx]
_1e3:	movzx  ecx,BYTE PTR [rdx+rax*1]     ; type = utf8d[byte]
_1e7:	test   edi,edi
_1e9:	je     _200                         ; *state != UTF8_ACCEPT ?
_1eb:	and    edx,0x3f                     ; byte & 0x3f
_1ee:	shl    esi,0x6                      ; *codep << 6
_1f1:	or     esi,edx                      ; |
_1f3:	jmp    _209
_200:	mov    esi,0xff                     ; :
_205:	shr    esi,cl                       ; 0xff >> type
_207:	and    esi,edx                      ; & byte
_209:	mov    DWORD PTR [rsp+0xc],esi      ; *codep =
_20d:	mov    edx,edi
_20f:	add    rcx,rdx                      ; *state + type
_212:	add    rcx,0x100                    ; + 256
_219:	movzx  ecx,BYTE PTR [rax+rcx*1]     ; *state = utf8d[...]
_21d:	test   ecx,ecx                      ; switch (decode(...))
_21f:	je     _1c0                         ; case UTF8_ACCEPT: goto FOUND
_221:	cmp    cl,0xc
_224:	je     _1a2                         ; state == UTF8_REJECT
_22a:	add    rbx,0x1                      ; default: ++b
_22e:	mov    edi,ecx                      ; prev = state
_230:	cmp    r14,rbx
_233:	jne    _1e0                         ; e != b
_235:	mov    rbx,r14                      ; b = e
_238:	jmp    _1c0
```

### GCC Bjoern
```nasm
_133:	mov    r10d,0xff
_139:	cmp    rbx,r12
_13c:	jb     _171                         ; b < e
_13e:	jmp    _1a9
_140:	and    edx,0x3f                     ; byte & 0x3f
_143:	shl    eax,0x6                      ; *codep << 6
_146:	or     eax,edx                      ; |
_148:	lea    edx,[rcx+rsi*1+0x100]        ; *state + type + 256
_14f:	movzx  ecx,BYTE PTR [r8+rdx*1]      ; *state = utf8d[...]
_154:	test   cl,cl                        ; switch (decode(...))
_156:	je     _19c                         ; case UTF8_ACCEPT: goto FOUND
_158:	cmp    cl,0xc
_15b:	jne    _1d8                         ; state != UTF8_REJECT
_15d:	mov    DWORD PTR [rsp+0xc],0xfffd   ; case UTF8_REJECT: codepoint = 0xfffd
_165:	cmp    esi,0x1
_168:	adc    rbx,0x0                      ; if (prev == UTF8_ACCEPT) ++b
_16c:	cmp    r12,rbx
_16f:	jbe    _1a9                         ; e <= b
_171:	xor    eax,eax                      ; prev = 0
_173:	xor    esi,esi                      ; state = 0
_175:	movzx  edi,BYTE PTR [rbx]
_178:	mov    ecx,edi
_17a:	mov    edx,edi
_17c:	movzx  ecx,BYTE PTR [r8+rcx*1]      ; type = utf8d[byte]
_181:	test   esi,esi
_183:	jne    _140                         ; *state != UTF8_ACCEPT ?
_185:	mov    eax,r10d                     ; :
_188:	lea    edx,[rcx+rsi*1+0x100]        ; *state + type + 256
_18f:	sar    eax,cl                       ; 0xff >> type
_191:	movzx  ecx,BYTE PTR [r8+rdx*1]      ; *state = utf8d[...]
_196:	and    eax,edi                      ; & byte
_198:	test   cl,cl
_19a:	jne    _158                         ; state != UTF8_ACCEPT
_19c:	mov    DWORD PTR [rsp+0xc],eax      ; FOUND: *codep =
_1a0:	add    rbx,0x1                      ; ++b
_1a4:	cmp    r12,rbx
_1a7:	ja     _171                         ; e > b
                                            ; exit point sandwiched here
_1d8:	lea    rdx,[rbx+0x1]                ; default: rdx = b + 1
_1dc:	cmp    r12,rdx
_1df:	jbe    _213                         ; e <= rdx
_1e1:	mov    esi,ecx                      ; prev = state
_1e3:	mov    rbx,rdx                      ; b = rdx
_1e6:	jmp    _175
_213:	mov    DWORD PTR [rsp+0xc],eax      ; *codep =
_217:	add    rbx,0x2                      ; b += 2
_21b:	jmp    _16c
```

## Two Tables
### Clang Two Tables
```nasm
_1c7:	jmp    _1ed
_1c9:	lea    rax,[rbx-0x1]                ; case UTF8_REJECT: rax = b - 1
_1cd:	test   esi,esi
_1cf:	mov    DWORD PTR [rsp+0xc],0xfffd   ; codepoint = 0xfffd
_1d7:	cmovne rbx,rax                      ; if (prev != UTF8_ACCEPT) b = rax
_1e0:	mov    QWORD PTR [rsp],r8           ; FOUND:
_1e4:	add    rbx,0x1                      ; ++b
_1e8:	cmp    rbx,r14
_1eb:	jae    _24e                         ; b >= e
_1ed:	mov    DWORD PTR [rsp+0xc],0x0      ; codepoint = 0
_1f5:	cmp    rbx,r14
_1f8:	jae    _1e0                         ; b >= e
_1fa:	xor    edi,edi                      ; prev = 0
_1fc:	xor    esi,esi                      ; state = 0
_200:	movzx  eax,BYTE PTR [rbx]
_203:	movzx  ecx,BYTE PTR [rax+r9*1]      ; type = utf8d[byte]
_208:	test   esi,esi
_20a:	je     _220                         ; *state != UTF8_ACCEPT ?
_20c:	and    eax,0x3f                     ; byte & 0x3f
_20f:	shl    edi,0x6                      ; *codep << 6
_212:	or     edi,eax                      ; |
_214:	jmp    _229
_220:	mov    edi,0xff                     ; :
_225:	shr    edi,cl                       ; 0xff >> type
_227:	and    edi,eax                      ; & byte
_229:	mov    DWORD PTR [rsp+0xc],edi      ; *codep =
_22d:	mov    eax,esi
_22f:	add    rax,rcx                      ; *state + type
_232:	movzx  eax,BYTE PTR [rdx+rax*1]     ; *state = utf8t[...]
_236:	test   eax,eax                      ; switch (decode(...))
_238:	je     _1e0                         ; case UTF8_ACCEPT: goto FOUND
_23a:	cmp    al,0xc
_23c:	je     _1c9                         ; state == UTF8_REJECT
_23e:	add    rbx,0x1                      ; default: ++b
_242:	mov    esi,eax                      ; prev = state
_244:	cmp    r14,rbx
_247:	jne    _200                         ; e != b
_249:	mov    rbx,r14                      ; b = e
_24c:	jmp    _1e0
```

### GCC Two Tables
```nasm
_133:	mov    r11d,0xff
_140:	cmp    rbx,r12
_143:	jb     _17d                         ; b < e
_145:	jmp    _1b1
_150:	and    edx,0x3f                     ; byte & 0x3f
_153:	shl    eax,0x6                      ; *codep << 6
_156:	or     eax,edx                      ; |
_158:	lea    edx,[rcx+rsi*1]              ; *state + type
_15b:	movzx  ecx,BYTE PTR [r8+rdx*1]      ; *state = utf8d[...]
_160:	test   cl,cl                        ; switch (decode(...))
_162:	je     _1a4                         ; case UTF8_ACCEPT: goto FOUND
_164:	cmp    cl,0xc
_167:	jne    _1e0                         ; state != UTF8_REJECT
_169:	mov    DWORD PTR [rsp+0xc],0xfffd   ; case UTF8_REJECT: codepoint = 0xfffd
_171:	cmp    esi,0x1
_174:	adc    rbx,0x0                      ; if (prev == UTF8_ACCEPT) ++b
_178:	cmp    r12,rbx
_17b:	jbe    _1b1                         ; e <= b
_17d:	xor    eax,eax                      ; prev = 0
_17f:	xor    esi,esi                      ; state = 0
_181:	movzx  edi,BYTE PTR [rbx]
_184:	mov    ecx,edi
_186:	mov    edx,edi
_188:	movzx  ecx,BYTE PTR [r9+rcx*1]      ; type = utf8d[byte]
_18d:	test   esi,esi
_18f:	jne    _150                         ; *state != UTF8_ACCEPT ?
_191:	mov    eax,r11d                     ; :
_194:	lea    edx,[rcx+rsi*1]              ; *state + type
_197:	sar    eax,cl                       ; 0xff >> type
_199:	movzx  ecx,BYTE PTR [r8+rdx*1]      ; *state = utf8t[...]
_19e:	and    eax,edi                      ; & byte
_1a0:	test   cl,cl
_1a2:	jne    _164                         ; state != UTF8_ACCEPT
_1a4:	mov    DWORD PTR [rsp+0xc],eax      ; FOUND: *codep =
_1a8:	add    rbx,0x1                      ; ++b
_1ac:	cmp    r12,rbx
_1af:	ja     _17d                         ; e > b
                                            ; exit point sandwiched here
_1e0:	lea    rdx,[rbx+0x1]                ; default: rdx = b + 1
_1e4:	cmp    r12,rdx
_1e7:	jbe    _233                         ; e <= rdx
_1e9:	mov    esi,ecx                      ; prev = state
_1eb:	mov    rbx,rdx                      ; b = rdx
_1ee:	jmp    _181
_233:	mov    DWORD PTR [rsp+0xc],eax      ; *codep =
_237:	add    rbx,0x2                      ; b += 2
_23b:	jmp    _178
```

## Mask LUT
### Clang Mask LUT
```nasm
_1c7:	jmp    _1ed
_1c9:	lea    rax,[rbx-0x1]                ; case UTF8_REJECT: rax = b - 1
_1cd:	test   esi,esi
_1cf:	mov    DWORD PTR [rsp+0xc],0xfffd   ; codepoint = 0xfffd
_1d7:	cmovne rbx,rax                      ; if (prev != UTF8_ACCEPT) b = rax
_1e0:	mov    QWORD PTR [rsp],r8           ; FOUND:
_1e4:	add    rbx,0x1                      ; ++b
_1e8:	cmp    rbx,r14
_1eb:	jae    _25d                         ; b >= e
_1ed:	mov    DWORD PTR [rsp+0xc],0x0      ; codepoint = 0
_1f5:	cmp    rbx,r14
_1f8:	jae    _1e0                         ; b >= e
_1fa:	xor    edi,edi                      ; prev = 0
_1fc:	xor    esi,esi                      ; state = 0
_200:	movzx  ecx,BYTE PTR [rbx]
_203:	movzx  edx,BYTE PTR [rcx+r10*1]     ; type = utf8d[byte]
_208:	test   esi,esi
_20a:	je     _220                         ; *state != UTF8_ACCEPT ?
_20c:	and    ecx,0x3f                     ; byte & 0x3f
_20f:	shl    edi,0x6                      ; *codep << 6
_212:	or     edi,ecx                      ; |
_214:	jmp    _22c
_220:	mov    edi,edx                      ; :
_222:	movzx  eax,BYTE PTR [rdi+r9*1]      ; mask_lut[type]
_227:	and    al,cl                        ; & byte
_229:	movzx  edi,al
_22c:	mov    DWORD PTR [rsp+0xc],edi      ; *codep =
_230:	mov    eax,esi
_232:	add    rax,rdx                      ; *state + type
_235:	add    rax,0x100                    ; + 256
_23b:	movzx  ecx,BYTE PTR [r10+rax*1]     ; *state = utf8d[...]
_240:	test   ecx,ecx                      ; switch (decode(...))
_242:	je     _1e0                         ; case UTF8_ACCEPT: goto FOUND
_244:	cmp    cl,0xc
_247:	je     _1c9                         ; state == UTF_REJECT
_24d:	add    rbx,0x1                      ; default: ++b
_251:	mov    esi,ecx                      ; prev = state
_253:	cmp    r14,rbx
_256:	jne    _200                         ; e != b
_258:	mov    rbx,r14                      ; b = e
_25b:	jmp    _1e0
```

### GCC Mask LUT
```nasm
_13a:	cmp    rbx,r12
_13d:	jb     _17c                         ; b < e
_13f:	jmp    _1b5
_148:	mov    eax,ecx
_14a:	and    edx,0x3f                     ; byte & 0x3f
_14d:	shl    eax,0x6                      ; *codep << 6
_150:	mov    ecx,edx
_152:	or     ecx,eax                      ; |
_154:	lea    eax,[rdi+rsi*1+0x100]        ; *state + type + 256
_15b:	movzx  eax,BYTE PTR [r8+rax*1]      ; *state = utf8d[...]
_160:	test   al,al                        ; switch (decode(...))
_162:	je     _1a8                         ; case UTF8_ACCEPT: goto FOUND
_164:	cmp    al,0xc
_166:	jne    _1e0                         ; state != UTF8_REJECT
_168:	mov    DWORD PTR [rsp+0xc],0xfffd   ; case UTF8_REJECT: codepoint = 0xfffd
_170:	cmp    esi,0x1
_173:	adc    rbx,0x0                      ; if (prev == UTF8_ACCEPT) ++b
_177:	cmp    r12,rbx
_17a:	jbe    _1b5                         ; e <= b
_17c:	xor    ecx,ecx                      ; prev = 0
_17e:	xor    esi,esi                      ; state = 0
_180:	movzx  edx,BYTE PTR [rbx]
_183:	movzx  edi,BYTE PTR [r8+rdx*1]      ; type = utf8d[byte]
_188:	mov    rax,rdx
_18b:	test   esi,esi
_18d:	jne    _148                         ; *state != UTF8_ACCEPT ?
_18f:	mov    edx,edi                      ; :
_191:	and    al,BYTE PTR [r10+rdx*1]      ; masklut[type] & byte
_195:	movzx  ecx,al
_198:	lea    eax,[rdi+rsi*1+0x100]        ; *state + type + 256
_19f:	movzx  eax,BYTE PTR [r8+rax*1]      ; *state = utf8d[...]
_1a4:	test   al,al
_1a6:	jne    _164                         ; state != UTF8_ACCEPT
_1a8:	mov    DWORD PTR [rsp+0xc],ecx      ; FOUND: *codep =
_1ac:	add    rbx,0x1                      ; ++b
_1b0:	cmp    r12,rbx
_1b3:	ja     _17c                         ; e > b
                                            ; exit point sandwiched here
_1e0:	lea    rdx,[rbx+0x1]                ; default: rdx = b + 1
_1e4:	cmp    r12,rdx
_1e7:	jbe    _233                         ; e <= rdx
_1e9:	mov    esi,eax                      ; prev = state
_1eb:	mov    rbx,rdx                      ; b = rdx
_1ee:	jmp    _180
_233:	mov    DWORD PTR [rsp+0xc],ecx      ; *codep =
_237:	add    rbx,0x2                      ; b += 2
_23b:	jmp    _177
```

## BIGBOI
### Clang BIGBOI
```nasm
_1c7:	jmp    _1ed
_1c9:	lea    rax,[rbx-0x1]                ; rax = b - 1
_1cd:	test   edi,edi
_1cf:	mov    DWORD PTR [rsp+0xc],0xfffd   ; codepoint = 0xfffd
_1d7:	cmovne rbx,rax                      ; if (prev != UTF8_ACCEPT) b = rax
_1e0:	mov    QWORD PTR [rsp],r8           ; FOUND:
_1e4:	add    rbx,0x1                      ; ++b
_1e8:	cmp    rbx,r14
_1eb:	jae    _253                         ; b >= e
_1ed:	mov    DWORD PTR [rsp+0xc],0x0      ; codepoint = 0
_1f5:	cmp    rbx,r14
_1f8:	jae    _1e0                         ; b >= e
_1fa:	xor    esi,esi                      ; prev = 0
_1fc:	xor    edi,edi                      ; state = 0
_200:	movzx  edx,BYTE PTR [rbx]
_203:	test   edi,edi
_205:	je     _220                         ; *state != UTF8_ACCEPT ?
_207:	mov    eax,edx
_209:	and    eax,0x3f                     ; byte & 0x3f
_20c:	shl    esi,0x6                      ; *codep << 6
_20f:	or     esi,eax                      ; |
_211:	jmp    _229
_220:	mov    eax,edx                      ; :
_222:	and    al,BYTE PTR [rdx+r9*1]       ; masklut[byte] & byte
_226:	movzx  esi,al
_229:	mov    DWORD PTR [rsp+0xc],esi      ; *codep =
_22d:	mov    eax,edi
_22f:	shl    rax,0x8                      ; *state << 8
_233:	or     rax,rdx                      ; + byte
_236:	movzx  edx,BYTE PTR [rcx+rax*1]     ; *state = utf8d[...]
_23a:	test   edx,edx                      ; switch (decode(...))
_23c:	je     _1e0                         ; case UTF8_ACCEPT: goto FOUND
_23e:	cmp    dl,0x1
_241:	je     _1c9                         ; state == UTF8_REJECT
_243:	add    rbx,0x1                      ; default: ++b
_247:	mov    edi,edx                      ; prev = state
_249:	cmp    r14,rbx
_24c:	jne    _200                         ; e != b
_24e:	mov    rbx,r14                      ; b = e
_251:	jmp    _1e0
```

### GCC BIGBOI
```nasm
_13a:	cmp    rbx,r12
_13d:	jb     _17e                         ; b < e
_148:	mov    edx,edi
_14a:	mov    eax,ecx
_14c:	and    edx,0x3f                     ; byte & 0x3f
_14f:	shl    eax,0x6                      ; *codep << 6
_152:	mov    ecx,edx
_154:	or     ecx,eax                      ; |
_156:	mov    eax,esi
_158:	shl    eax,0x8                      ; *state << 8
_15b:	add    eax,edi                      ; + byte
_15d:	movzx  eax,BYTE PTR [r8+rax*1]      ; *state = utf8d[...]
_162:	test   al,al                        ; switch (decode(...))
_164:	je     _1a4                         ; case UTF8_ACCEPT: goto FOUND
_166:	cmp    al,0x1
_168:	jne    _1e0
_16a:	mov    DWORD PTR [rsp+0xc],0xfffd   ; case UTF8_REJECT: codepoint = 0xfffd
_172:	cmp    esi,0x1
_175:	adc    rbx,0x0                      ; if (prev == UTF8_ACCEPT) ++b
_179:	cmp    r12,rbx
_17c:	jbe    _1b1                         ; e <= b
_17e:	xor    ecx,ecx                      ; prev = 0
_180:	xor    esi,esi                      ; state = 0
_182:	movzx  edi,BYTE PTR [rbx]
_185:	mov    eax,edi
_187:	test   esi,esi
_189:	jne    _148                         ; *state != UTF8_ACCEPT ?
_18b:	mov    edx,edi                      ; :
_18d:	and    al,BYTE PTR [r10+rdx*1]      ; masklut[byte] & byte
_191:	movzx  ecx,al
_194:	mov    eax,esi
_196:	shl    eax,0x8                      ; *state << 8
_199:	add    eax,edi                      ; + byte
_19b:	movzx  eax,BYTE PTR [r8+rax*1]      ; *state = utf8d[...]
_1a0:	test   al,al                        ; switch (decode(...))
_1a2:	jne    _166                         ; state != UTF8_ACCEPT
_1a4:	mov    DWORD PTR [rsp+0xc],ecx      ; FOUND: *codep =
_1a8:	add    rbx,0x1                      ; ++b
_1ac:	cmp    r12,rbx
_1af:	ja     _17e                         ; e > b
                                            ; exit point sandwiched here
_1e0:	lea    rdx,[rbx+0x1]                ; default: rdx = b + 1
_1e4:	cmp    r12,rdx
_1e7:	jbe    _233                         ; e <= rdx
_1e9:	mov    esi,eax                      ; prev = state
_1eb:	mov    rbx,rdx                      ; b = rdx
_1ee:	jmp    _182
_1f0:	mov    edx,eax
_233:	mov    DWORD PTR [rsp+0xc],ecx      ; *codep =
_237:	add    rbx,0x2                      ; b += 2
_23b:	jmp    _179
```
