#if 0
set -exo pipefail
for i in NAIVE BJOERN TWOTBLS BIGBOI MASKLUT;  do
    gcc -g -I3rdparty -D$i -DTEST_OUT="\"test_$i.txt\"" -o test_$i test.c
    ./test_$i
    diff medium.utf32 test_$i.txt
done
exit
# @Tools:
# uconv -f "utf-8" -t "utf-32le" --callback substitute -o medium.utf32 medium.txt
# radiff2 -x medium.utf32 test_$i.txt | less -r
#endif
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"
#include "sugar.h"
#if defined (NAIVE)
#include "naive.c"
#elif defined (BJOERN)
#include "bjoern.c"
#elif defined (TWOTBLS)
#include "twotbls.c"
#elif defined (BIGBOI)
#include "bigboi.c"
#elif defined (MASKLUT)
#include "masklut.c"
#endif
#define TEST_FILE "medium.txt"
int main (int argc, char ** argv)
{
    struct stat st;
    stat(TEST_FILE, &st);
    FILE * r = fopen(TEST_FILE, "rb");
    u8 * b = malloc(st.st_size);
    u8 * e = b + st.st_size;
    fread(b, 1, st.st_size, r);
    u32 * c = NULL;
    FILE * w = fopen(TEST_OUT, "wb");
#if defined (NAIVE)
    c = utf8_to_utf32(b, st.st_size);
#else
    arrsetcap(c, st.st_size);
    for (; b < e; ++b)
    {
        u32 codepoint = 0;
        for (u32 prev = 0, state = 0; b < e; ++b)
        {
            switch (decode(&state, &codepoint, cast (u32)*b))
            {
                case UTF8_REJECT:
                    codepoint = 0xfffd;
                    if (prev != UTF8_ACCEPT)
                        --b;
                    /* fall through */
                case UTF8_ACCEPT:
                    goto FOUND;
                default:
                    prev = state;
                    break;
            }
        }
        lbl FOUND: arrpush(c, codepoint);
    }
#endif
    fwrite(c, 1, arrlen(c)*sizeof (*c), w);
    return 0;
}
