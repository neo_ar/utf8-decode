#pragma once
//@SECTION: headers
#include <stdint.h>
//@SECTION: syntax sugar
#define ARRLEN(a) (sizeof (a)/sizeof (*a))
#define cast
#define forTas(T, a, s) for (T * i = a, * e = i + ARRLEN(a); i < e; i += s)
#define lbl
#define persistent static
#define proc static
//@SECTION: typedefs
#define s32 int32_t
#define u8 uint8_t
#define u32 uint32_t
