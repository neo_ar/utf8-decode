/* This software is available under 2 licenses -- choose whichever you prefer.
 *
 * ALTERNATIVE A - MIT License
 * Copyright (c) 2019 Mio Iwakura
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ALTERNATIVE B - Public Domain (www.unlicense.org)
 * This is free and unencumbered software released into the public domain.
 * Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
 * software, either in source code form or as a compiled binary, for any purpose,
 * commercial or non-commercial, and by any means.
 * In jurisdictions that recognize copyright laws, the author or authors of this
 * software dedicate any and all copyright interest in the software to the public
 * domain. We make this dedication for the benefit of the public at large and to
 * the detriment of our heirs and successors. We intend this dedication to be an
 * overt act of relinquishment in perpetuity of all present and future rights to
 * this software under copyright law.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. */

//@NOTE: This was my initial attempt at writing a UTF-8
//       decoder before I found Bjoern Hoehrmann's implementation.
//       Use his instead! http://bjoern.hoehrmann.de/utf-8/decoder/dfa/

//@SECTION: headers
#include "sugar.h"
//@SECTION: constants
#define UCS_CP_REPLACEMENT_CHAR 0xFFFD
#define UTF8_CONTINUATION_MAX 0xBF
#define UTF8_CONTINUATION_MIN 0x80
//@SECTION: tables
u8 utf8_mask_lut [4] = {[1]=0x1F, 0xF, 0x7};
u8 utf8_prefix_lut [256] = //@MATH: 1 << 8
{
    [0xC2]=1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3, 3
};
//@SECTION: code
proc void utf8_to_utf32(char * i, u32 l)
{
    char * e = i + l;
    u32 maybe_invalid_max = UTF8_CONTINUATION_MAX;
    u32 maybe_invalid_min = UTF8_CONTINUATION_MIN;
    while (i < e)
    {
        u8 codeunit = cast (u8)*i;
        if (codeunit <= 0x7F)
        {
            escape(&codeunit);
            ++i;
            continue;
        }
        u32 continuation_length = cast (u32)utf8_prefix_lut[codeunit];
        u32 codepoint = cast (u32)codeunit & utf8_mask_lut[continuation_length];
        switch (codeunit)
        {
            case 0xE0:
                maybe_invalid_min = 0xA0;
                goto MAYBE_INVALID;
            case 0xED:
                maybe_invalid_max = 0x9F;
                goto MAYBE_INVALID;
            case 0xF0:
                maybe_invalid_min = 0x90;
                goto MAYBE_INVALID;
            case 0xF4:
                maybe_invalid_max = 0x8F;
                goto MAYBE_INVALID;
            default:
                break;
        }
        u8 continuation;
        switch (continuation_length)
        {
            case 3:
                continuation = cast (u8)*++i;
                if (!continuation || continuation < UTF8_CONTINUATION_MIN
                        || continuation > UTF8_CONTINUATION_MAX)
                    goto INVALID_CODEPOINT;
                codepoint = codepoint<<6 | cast (u32)continuation&0x3F;
                /* fall through */
            case 2:
                lbl CASE2:
                continuation = cast (u8)*++i;
                if (!continuation || continuation < UTF8_CONTINUATION_MIN
                        || continuation > UTF8_CONTINUATION_MAX)
                    goto INVALID_CODEPOINT;
                codepoint = codepoint<<6 | cast (u32)continuation&0x3F;
                /* fall through */
            case 1:
                lbl CASE1:
                continuation = cast (u8)*++i;
                if (!continuation || continuation < UTF8_CONTINUATION_MIN
                        || continuation > UTF8_CONTINUATION_MAX)
                    goto INVALID_CODEPOINT;
                codepoint = codepoint<<6 | cast (u32)continuation&0x3F;
                break;
            case 0:
                ++i;
                goto INVALID_CODEPOINT;
        }
        escape(&codepoint);
        ++i;
        continue;
        lbl INVALID_CODEPOINT:
        codepoint = UCS_CP_REPLACEMENT_CHAR;
        escape(&codepoint);
        continue;
        lbl MAYBE_INVALID:
        continuation = cast (u8)*++i;
        if (!continuation || continuation < maybe_invalid_min
                || continuation > maybe_invalid_max)
        {
            maybe_invalid_max = UTF8_CONTINUATION_MAX;
            maybe_invalid_min = UTF8_CONTINUATION_MIN;
            goto INVALID_CODEPOINT;
        }
        codepoint = codepoint<<6 | cast (u32)continuation&0x3F;
        maybe_invalid_max = UTF8_CONTINUATION_MAX;
        maybe_invalid_min = UTF8_CONTINUATION_MIN;
        switch (continuation_length)
        {
            case 3: goto CASE2;
            case 2: goto CASE1;
        }
    }
}
/*                                                                           @TY
 * ### Contributors
 * * Conversion Routines
 *     * Mio Iwakura (utf8_to_utf32)
 * * Inspiration / Special Thanks
 *     * knight666 (utf8rewind)
 *     * The Unicode Consortium (The Unicode Standard v12.0)
 *     * WHATWG (Encoding Standard)
 *     * Sean Barrett (stb libs)
 *     * All my patrons (motivation/funding) */
